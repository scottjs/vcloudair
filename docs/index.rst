.. vCloudAir documentation master file, created by
   sphinx-quickstart on Thu Sep 29 10:25:59 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python vcloudair SDK Documentation
==================================

If you've made it this far, you've probably struggled using vCloud Air's /
vCloud Director's API. I can't say I blame you. It's awfully cumbersome! That's
why I wrote this small SDK for Python. I was hoping to make certain operations
easier and more pythonic.

Sure, the ``pyvcloudair`` package exists. But it brings along with it all the
complexity of the normal vCD API. Also, it hasn't been updated in a while
(at the time of writing this at least) and so it misses out on the new features
that VMware have released into the vCloud Air API. Hopefully that's where this
package can be of use.

This isn't a user guide. It's meant for developers (as this IS an SDK) and the
README file has some examples to get you going. The basic workflow is as
follows:

1. Instantiate one of the Session classes
2. Call the ``login()`` method
3. Log into a specific vCD instance
4. Instantiate one of the other classes in the package
5. Pass the data returned from logging in to the new class
6. Enjoy an easier time accessing the API features via Python

License
-------

This package is released under the MIT license and all the fun perks and
safeguards that come with that. Full license is available on the Git repo.

If you decide to use this to automate some fun things (maybe even your DR
system), don't blame me if VMware decides to make breaking changes to the
API and not tell anyone. It does tend to happen...

API Reference:
--------------

.. toctree::
   :maxdepth: 3

   vcloudair

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

